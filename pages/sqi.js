import { Breadcrumb, Button, Col, Form, Row, Table } from 'antd'
import React, { useRef } from 'react'
import datalisttypejual from '../data/datalisttypejual'
import DateField from '../komponen/DateField'
import SelectField from '../komponen/SelectField'
import Template2 from '../komponen/Template2'
import TextField from '../komponen/TextField'
import moment from 'moment'
import CheckField from '../komponen/CheckField'
import { PrinterOutlined, FileExcelOutlined, PaperClipOutlined } from '@ant-design/icons'
import { useReactToPrint } from 'react-to-print'
import withAuth from '../rute/withAuth'
import ReactHTMLTableToExcel from 'react-html-table-to-excel'
import withPrivateRoute from '../rute/withPrivateRoute'
import TableData from '../komponen/TableData'
import TableList from '../komponen/TableList'
import Text from 'antd/lib/typography/Text'
import datacolsq from '../data/datacolsq'
import datalistsq from '../data/datalistsq'

const sqi = () => {
    return (
        <>
            <Template2 kunci="sqi" judul={<Judul />} konten={<Konten />} />
        </>
    )
}

const Judul = () => {
    return (
        <>
            <Breadcrumb separator=">">
                <Breadcrumb.Item>Sales</Breadcrumb.Item>
                <Breadcrumb.Item>inquiries and Report</Breadcrumb.Item>
                <Breadcrumb.Item href="/sqe">Sales Quotation Inquiry</Breadcrumb.Item>
            </Breadcrumb>
        </>
    )
}

const Konten = () => {

    const ComponentRef = useRef(null)
    const handlePrint = useReactToPrint({
        content: () => ComponentRef.current
    })

    let onFinish = (values) => {
        console.log(values)
    }

    return (
        <>
            <Form
                name="sqe"
                onFinish={onFinish}
            >

                <Row gutter={16}>
                    <Col xs={24} sm={24} md={12} lg={8}>
                        <TextField name="id" required={true} message="Please input your #!" plchold="input #" />
                        <TextField name="ref" required={true} message="Please input your ref!" plchold="input ref" />
                        <DateField name="from" dataField={moment()} />
                        <DateField name="to" dataField={moment()} />
                        <SelectField name="kab" plchold="Input Kabupaten" dataField={datalisttypejual} />
                        <SelectField name="kec" plchold="Input Kecamatan" dataField={datalisttypejual} />
                    </Col>
                    <Col xs={24} sm={24} md={12} lg={8}>
                        <SelectField name="status" plchold="Input Status" dataField={datalisttypejual} />
                        <SelectField name="clsofgd" plchold="Input Class Of Goods" dataField={datalisttypejual} />
                        <SelectField name="category" plchold="Input Category" dataField={datalisttypejual} />
                        <TextField name="item" required={true} message="Please input your item!" plchold="input item" />
                        <SelectField name="items" plchold="Input Items" dataField={datalisttypejual} />
                        <SelectField name="typejual" plchold="Input Type Jual" dataField={datalisttypejual} />
                    </Col>
                    <Col xs={24} sm={24} md={12} lg={8}>
                        <SelectField name="customers" plchold="Input Customers" dataField={datalisttypejual} />
                        <SelectField name="koorsales" plchold="Input Koordinator Sales" dataField={datalisttypejual} />
                        <SelectField name="usersales" plchold="Input User Sales" dataField={datalisttypejual} />
                        <CheckField name="showall" title="Show All" dataField={false} />
                        <CheckField name="showallcred" title="Show All Credit Status" dataField={false} />
                    </Col>
                </Row>

                <Form.Item>
                    <Button type="primary" htmlType="submit">
                        Submit
                    </Button>
                </Form.Item>

            </Form>

            <div style={{ border: '1px solid rgba(0,0,0,.06)', padding: '5px', boxShadow: 'rgba(99, 99, 99, 0.2) 0px 2px 8px 0px' }}>
                <Button onClick={handlePrint}><PrinterOutlined /></Button>

                <ReactHTMLTableToExcel
                    id="test-table-xls-button"
                    className="download-table-xls-button"
                    table="table-to-xls"
                    filename="tablexls"
                    sheet="tablexls"
                    buttonText='.xls'
                />

                <TableList idXls="table-to-xls" colField={datacolsq} dataField={datalistsq} attr="title" />
                <Table ref={ComponentRef} columns={datacolsq} dataSource={datalistsq} scroll={{ x: 'max-content' }} size="small"
                    summary={pageData => {
                        let totalPrice = 0;
                        pageData.forEach(({ price }) => {
                            totalPrice += price;
                        });

                        return (
                            <>
                                <Table.Summary.Row>
                                    {/* <Table.Summary.Cell colSpan={9}></Table.Summary.Cell> */}
                                    <Table.Summary.Cell>Total</Table.Summary.Cell>
                                    <Table.Summary.Cell>
                                        <Text type="danger">{totalPrice}</Text>
                                    </Table.Summary.Cell>
                                </Table.Summary.Row>
                            </>
                        );
                    }}
                />

            </div>
        </>
    )
}

export default sqi