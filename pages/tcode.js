import React from 'react'
import { Breadcrumb, Button, Form, Input } from 'antd'
import SearchSelectField from '../komponen/SearchSelectField'
import { SearchOutlined } from '@ant-design/icons'
import datalistmenu from '../data/datalistmenu'
import { useRouter } from 'next/router'
import Template2 from '../komponen/Template2'
import withPrivateRoute from '../rute/withPrivateRoute'

const tcode = () => {

  // useEffect(() => {
  //   const timer = setTimeout(() => {
  //     console.log('This will run after 20 second! untuk timer session timeout')
  //   }, 20000);
  //   return () => clearTimeout(timer);
  // }, []);

  return (
    <>
      <Template2 kunci="tcode" judul={<Judul />} konten={<Konten />} />
    </>
  )
}

const Judul = () => {
  return (
    <>
      <Breadcrumb separator=">">
        <Breadcrumb.Item href="/tcode">T-code</Breadcrumb.Item>
      </Breadcrumb>
    </>
  )
}

const Konten = () => {
  const Router = useRouter()
  let onFinish = (values) => {
    Router.replace("/" + values.tcode)
  }
  return (
    <>
      <Form
        name="tcode"
        onFinish={onFinish}
        autoComplete="off"
      >
        <h3>Quick Menu</h3>
        <Input.Group compact>
          <SearchSelectField width='calc(100% - 45px)' name="tcode" plchold="Input Menu" dataField={datalistmenu} />
          <Button type="primary" htmlType="submit">
            <SearchOutlined style={{ fontSize: 12 }} />
          </Button>
        </Input.Group>
      </Form>
    </>
  )
}

// tcode.getInitialProps = async props => {
//   // notification.open({
//   //   message: 'Congratulations!',
//   //   description:
//   //     'You are authorized!',
//   //   icon: <SmileOutlined style={{ color: 'red' }} />,
//   // });
//   console.info('##### Congratulations! You are authorized! ######', props);
//   return {};
// };

export default withPrivateRoute(tcode)