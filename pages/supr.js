import React from 'react'
import { Breadcrumb, Table } from 'antd'
import Template2 from '../komponen/Template2'
import Grafik from '../komponen/Grafik'
import dataSuppliersRank from '../data/dataSuppliersRank'
import dataColSuppliersRank from '../data/dataColSuppliersRank'

const supr = () => {

    return (
        <>
            <Template2 kunci="supr" judul={<Judul />} konten={<Konten />} />
        </>
    )
}

const Judul = () => {
    return (
        <>
            <Breadcrumb separator=">">
                <Breadcrumb.Item>Sales</Breadcrumb.Item>
                <Breadcrumb.Item href="/custr">Customers Ranking</Breadcrumb.Item>
            </Breadcrumb>
        </>
    )
}

const Konten = () => {
    return (
        <>
            <Table dataSource={dataSuppliersRank} columns={dataColSuppliersRank} size="small"  scroll={{ x: 'max-content' }} pagination={false} bordered={true} />
            <Grafik dataField={dataSuppliersRank} x="supplier" y="amount" title="Top 10 Suppliers In Fiscal Year" />
        </>
    )
}

export default supr