import React from 'react'
import { Breadcrumb, Col, Form, Row, Button } from 'antd'
import Template2 from '../komponen/Template2'
import TextField from '../komponen/TextField'
import NumberField from '../komponen/NumberField'
import DateField from '../komponen/DateField'
import datalistcustomers from '../data/datalistcustomers'
import SearchSelectField from '../komponen/SearchSelectField'
import datalisttypejual from '../data/datalisttypejual'
import SelectField from '../komponen/SelectField'
import moment from 'moment'
import withPrivateRoute from '../rute/withPrivateRoute'

const sqe = () => {
  return (
    <Template2 kunci="sqe" judul={<Judul />} konten={<Konten />} />
  )
}

const Judul = () => {
  return (
    <>
      <Breadcrumb separator=">">
        <Breadcrumb.Item>Sales</Breadcrumb.Item>
        <Breadcrumb.Item>Transaction</Breadcrumb.Item>
        <Breadcrumb.Item href="/sqe">Sales Quotation Entry</Breadcrumb.Item>
      </Breadcrumb>
    </>
  )
}

const Konten = () => {
  let onFinish = (values) => {
    console.log(values)
  }

  return (
    <>
      <Form
        name="sqe"
        onFinish={onFinish}
      >

        <Row gutter={16}>
          <Col xs={24} sm={24} md={12} lg={8}>
            <SelectField name="typejual" plchold="Input Type Jual" dataField={datalisttypejual} />
            <SearchSelectField name="customers" plchold="Input Customer" dataField={datalistcustomers} />
            <TextField name="username" required={true} message="Please input your username!" plchold="input username" />
          </Col>
          <Col xs={24} sm={24} md={12} lg={8}>
            <NumberField name="angka"/>
            <DateField name="tanggal" dataField={moment()} />
            <TextField name="username2" required={true} message="Please input your username!" plchold="input username" />
          </Col>
          <Col xs={24} sm={24} md={12} lg={8}>
            <TextField name="username1" required={true} message="Please input your username!" plchold="input username" />
          </Col>
        </Row>

        <Form.Item>
          <Button type="primary" htmlType="submit">
            Submit
          </Button>
        </Form.Item>

      </Form>
    </>
  )
}

export default withPrivateRoute(sqe)