import React from 'react'
import { Breadcrumb, Table } from 'antd'
import Template2 from '../komponen/Template2'
import dataCustomersRank from '../data/dataCustomersRank'
import dataColCustomersRank from '../data/dataColCustomersRank'
import Grafik from '../komponen/Grafik'

const custr = () => {

    return (
        <>
            <Template2 kunci="custr" judul={<Judul />} konten={<Konten />} />
        </>
    )
}

const Judul = () => {
    return (
        <>
            <Breadcrumb separator=">">
                <Breadcrumb.Item>Sales</Breadcrumb.Item>
                <Breadcrumb.Item href="/custr">Customers Ranking</Breadcrumb.Item>
            </Breadcrumb>
        </>
    )
}

const Konten = () => {
    return (
        <>
            <Table dataSource={dataCustomersRank} columns={dataColCustomersRank} size="small"  scroll={{ x: 'max-content' }} pagination={false} bordered={true} />
            <Grafik dataField={dataCustomersRank} x="customer" y="amount" title="Top 10 Customers In Fiscal Year" />
        </>
    )
}

export default custr