import { Button } from 'antd'
import React, { useRef } from 'react'
import { useReactToPrint } from 'react-to-print'

const PdfTemplate = () => {
    const ComponentRef = useRef(null)

    const handlePrint = useReactToPrint({
        content: () => ComponentRef.current
    })
    return (
        <>
            <Button onClick={handlePrint}>Cetak</Button>
            <div ref={ComponentRef} style={{ margin: '80px' }}>
                <img src="https://tribintangemasmulia.com/assets/img/tem/logo_tem.png" width="70" height="70" />
                <p style={{ float: 'right' }}>SALES QUOTATION</p>
                <p>PT TRIBINTANG EMAS MULIA</p>
                <p style={{ maxWidth: '45%' }}>KEBAK JETIS RT.01 RW.09 NANGSRI, KEBAK KRAMAT, KAB.
                    KARANGANYAR, JAWA TENGAH</p>
                <div style={{ float: 'right' }}>
                    <p>Date 31/03/2022</p>
                    <p>Quotation No. 02-22-2000008784</p>
                </div>
                <div>
                    <p>Telp. +62 271 821491</p>
                    <p>JAWA TENGAH</p>
                </div>

                <p>Fax +62 271 633108</p>
                <p>Email kepsgslo@yahoo.com</p>
                <hr />
                <div style={{ float: 'right' }}>
                    <p>Delivered To</p>
                    <p>WARIS TB</p>
                    <p>JL. TALUN, MUNTILAN, MAGELANG</p>
                </div>
                <div>
                    <p>Charge To</p>
                    <p>WARIS TB</p>
                    <p>JL. TALUN, MUNTILAN, MAGELANG</p>
                </div>
                <table border="1" width="100%">
                    <thead>
                        <tr>
                            <th>Customer's Reference</th>
                            <th>Sales Person</th>
                            <th>Your VAT no</th>
                            <th>Our Quotation No</th>
                            <th>Valid until</th>
                        </tr>
                    </thead>
                    <tbody align="center">
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>84577</td>
                            <td>07/04/2022</td>
                        </tr>
                    </tbody>
                </table>
                <i>Payment Terms: Tunai</i>
                <table border="1" width="100%">
                    <thead>
                        <tr>
                            <th>Item Code</th>
                            <th>Item Description / SO</th>
                            <th>Quantity Unit</th>
                            <th>Price</th>
                            <th>Discount %</th>
                            <th>Total</th>
                        </tr>
                    </thead>
                    <tbody align="center">
                        <tr>
                            <td>147001</td>
                            <td>SEMEN PADANG PCC 40 KG</td>
                            <td>5 ZAK</td>
                            <td>40,500.00</td>
                            <td>0</td>
                            <td>202,500</td>
                        </tr>
                        <tr>
                            <td colSpan={6}>from android</td>
                        </tr>
                        <tr>
                            <td rowSpan={5} colSpan={4}></td>
                            <td>Sub-total</td>
                            <td>202,500</td>
                        </tr>
                        <tr>
                            <td>Shipping</td>
                            <td>0</td>
                        </tr>
                        <tr>
                            <td>DPP</td>
                            <td>184,090.91</td>
                        </tr>
                        <tr>
                            
                            <td>PPN (10%)</td>
                            <td>18,409</td>
                        </tr>
                        <tr>
                            <td>TOTAL ORDER VAT INCL</td>
                            <td>202,500</td>
                        </tr>
                    </tbody>
                </table>
                <p align="center">All amounts stated in - IDR</p>
                <p align="center">Bank: KAS KECIL NANGSRI KEBAKKRAMAT KRA, Bank Account:</p>
            </div>

        </>
    )
}

export default PdfTemplate