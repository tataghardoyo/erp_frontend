import React, { useEffect } from 'react'
import { Form, notification, Space } from 'antd'
import { UserOutlined, LoginOutlined, LockOutlined, FrownOutlined, SmileOutlined } from '@ant-design/icons'
import sampleImage from "./img/login.jpg"
import { useRouter } from "next/router"
import Head from 'next/head'
import TextField from '../komponen/TextField'
import PasswordField from '../komponen/PasswordField'
import Image from "next/image"
import sampleLogo from "./img/logo.png"
import Title from 'antd/lib/typography/Title'
import noAuth from '../rute/noAuth'

const Home = () => {
  const Router = useRouter()

  const onFinish = (values) => {
    if (values.username === "grego" && values.password === "1234") {
      localStorage.setItem("accessToken", "initoken")
      Router.replace("/tcode")
      notification.open({
        message: 'Congratulations!',
        description:
          'You are authorized!',
        icon: <SmileOutlined style={{ color: 'green' }} />,
      });
    } else {
      notification.open({
        message: 'Failed Sign In',
        description:
          'Username or password incorrect',
        icon: <FrownOutlined style={{ color: 'red' }} />,
      });
    }

  }

  return (
    <>
      <Head>
        <title>ERP</title>
        <meta name="description" content="ERP" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <div style={{ backgroundImage: `url(${sampleImage.src})`, backgroundRepeat: 'no-repeat', backgroundSize: '100% 100%', width: '100%', height: '100vh', padding: '0px 20px' }}>
        <div className='pembungkus-login'>
          <div style={{ color: 'white', textAlign: 'center' }}>
            <Image src={sampleLogo} width="60" height="30" />
          </div>
          <div>
            <Title level={3} style={{ textAlign: 'center', color: 'white' }}>Welcome to the App</Title>
          </div>
          <div style={{ textAlign: 'center', color: 'white' }}>Silahkan Masuk untuk menggunakan Sistem !!!</div>

          <div className='pembungkus-form-login'>
            <Form
              name="normal_login"
              className="login-form"
              onFinish={onFinish}
            >
              <TextField name="username" required={true} message="Please input your username!" prefix={<UserOutlined />} plchold="input username" />
              <PasswordField name="password" required={true} message="Please input your password!" prefix={<LockOutlined />} plchold="input password" />

              <Form.Item>
                <button className='tombol-login'>
                  <LoginOutlined />  Sign In
                </button>
              </Form.Item>

            </Form>
          </div>
        </div>
      </div>
    </>
  )
}

export default Home