import { Form, Select } from 'antd'
import React from 'react'

const SearchSelectField = (props) => {
    return (
        <>
            <Form.Item
                label={props.label}
                name={props.name}
                rules={[{ required: props.required, message: props.message }]}
                labelCol={{ span: props.labelcol }}
                wrapperCol={{ span: props.wrappercol }}
                style={{ width: props.width }}
            >
                <Select
                    showSearch
                    placeholder={props.plchold}
                    onChange={props.onchange}
                    options={props.dataField}
                    filterOption={(input, option) =>
                        option.label.toLowerCase().indexOf(input.toLowerCase()) >= 0
                    }
                />
            </Form.Item>
        </>
    )
}

export default SearchSelectField