import React from "react";

export default class TableList extends React.Component {

    state = {
        kolom: this.props.colField.map((item)=>item[this.props.attr]).filter((item)=> item !== "Aksi"),
        columns: [],
        columnsToHide: ["key"],
        results: this.props.dataField
    }

    componentDidMount() {
        this.mappDynamicColumns();
    }

    mappDynamicColumns = () => {
        let columns = [];
        this.state.results.forEach((result) => {
            Object.keys(result).forEach((col) => {
                if (!columns.includes(col)) {
                    columns.push(col);
                }
            });
            this.setState({ columns });
        });
    };

    addTableRow = (result) => {
        let row = [];
        this.state.columns.forEach((col) => {
            if (!this.state.columnsToHide.includes(col)) {
                row.push(
                    Object.keys(result).map((item) => {
                        if (result[item] && item === col) {
                            return result[item];
                        } else if (item === col) {
                            return "No Value";
                        }
                    })
                );
                row = this.filterDeepUndefinedValues(row);
            }
        });

        return row.map((item, index) => {
            return (
                <td
                    key={`${item}--${index}`}
                >
                    {item}
                </td>
            );
        });
    };

    mapTableColumns = () => {
        return this.state.kolom.map((col) => {
            return (
                <th
                    align='left'
                    key={col}
                    scope="col"
                >
                    {col}
                </th>
            );
        });
    };

    filterDeepUndefinedValues = (arr) => {
        return arr
            .map((val) =>
                val.map((deepVal) => deepVal).filter((deeperVal) => deeperVal)
            )
            .map((val) => {
                if (val.length < 1) {
                    val = ["-"];
                    return val;
                }
                return val;
            });
    };

    createTable = (results) => {
        return (
            <table id={this.props.idXls}>
                <thead>
                    <tr>
                        <th align='left' colSpan={this.state.kolom.length}>Search All Sales Quotation</th>
                    </tr>
                    <tr>
                        <td align='left'>Print Out Date</td>
                        <td align='left' colSpan={this.state.kolom.length - 1}>20-20-20</td>
                    </tr>
                    <tr>
                        <td align='left'>Fiscal Year</td>
                        <td align='left' colSpan={this.state.kolom.length - 1}>20-20-20</td>
                    </tr>
                    <tr>
                        <td align='left'>Date</td>
                        <td align='left' colSpan={this.state.kolom.length - 1}>20-20-20</td>
                    </tr>
                    <tr>{this.mapTableColumns()}</tr>
                </thead>
                <tbody>
                    {results.map((result, index) => {
                        return <tr key={result.key}>{this.addTableRow(result)}</tr>;
                    })}
                </tbody>
            </table>
        );
    };

    render() {
        return (
            <div style={{ display: 'none' }}>
                {this.state.results.length ? (
                    <div>
                        {this.createTable(this.state.results)}
                    </div>
                ) : null}
            </div>
        );
    }
}
