import { Form, Input } from 'antd'
import React from 'react'

const TextField = (props) => {
    return (
        <>
            <Form.Item
                name={props.name}
                label={props.label}
                initialValue={props.dataField}
                rules={[{ required: props.required, message: props.message }]}
                labelCol={{ span: props.labelcol }}
                wrapperCol={{ span: props.wrappercol }}
            >
                <Input
                    prefix={props.prefix}
                    placeholder={props.plchold}
                />
            </Form.Item>
        </>
    )
}

export default TextField