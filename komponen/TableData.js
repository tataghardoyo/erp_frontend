import React from 'react'

const TableData = (props) => {
    let getHeader = () => {
        var keys = Object.keys(props.data[0])
        return keys.map((key, index) => {
            return <th style={{ padding: '4px 4px' }} key={key}>{key.toUpperCase()}</th>
        })
    }
    let getRowsData = () => {
        var items = props.data;
        var keys = Object.keys(props.data[0])
        return items.map((row, index) => {
            return <tr key={index}><RenderRow key={index} data={row} keys={keys} /></tr>
        })
    }
    return (
        <div style={{ overflow: 'auto hidden', display: 'none' }}>
            <table id={props.idXls} style={{ width: 'max-content', minWidth: '100%', tableLayout: 'auto' }}>
                <thead style={{ background: '#fafafa', textAlign: 'left' }}>
                    <tr>
                        <th align='left' colSpan={7}>Search All Sales Quotation</th>
                    </tr>
                    <tr>
                        <td align='left'>Print Out Date</td>
                        <td align='left' colSpan={6}>20-20-20</td>
                    </tr>
                    <tr>
                        <td align='left'>Fiscal Year</td>
                        <td align='left' colSpan={6}>20-20-20</td>
                    </tr>
                    <tr>
                        <td align='left'>Date</td>
                        <td align='left' colSpan={6}>20-20-20</td>
                    </tr>
                    <tr>{getHeader()}</tr>
                </thead>
                <tbody>
                    {getRowsData()}
                </tbody>
            </table>
        </div>
    )
}
const RenderRow = (props) => {
    return props.keys.map((key, index) => {
        return <td style={{ width: '150px', padding: '4px 4px' }} key={props.data[key]}>{props.data[key]}</td>
    })
}
export default TableData