import { Form, Select } from 'antd'
import React from 'react'

const SelectField = (props) => {
    return (
        <>
            <Form.Item
                label={props.label}
                name={props.name}
                rules={[{ required: props.required, message: props.message }]}
                labelCol={{ span: props.labelcol }}
                wrapperCol={{ span: props.wrappercol }}
                style={{ width: props.width }}
            >
                <Select
                    placeholder={props.plchold}
                    onChange={props.onchange}
                    options={props.dataField}
                />
            </Form.Item>
        </>
    )
}

export default SelectField