import { Checkbox, Form } from 'antd'
import React from 'react'

const CheckField = (props) => {
    return (
        <>
            <Form.Item name={props.name} valuePropName="checked" initialValue={props.dataField}>
                <Checkbox>{props.title}</Checkbox>
            </Form.Item>
        </>
    )
}

export default CheckField