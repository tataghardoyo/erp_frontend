import React, { useState, useEffect } from 'react'
import { useRouter } from 'next/router'
import { Dropdown, Layout, Menu, notification } from 'antd';
import {
    MenuUnfoldOutlined,
    MenuFoldOutlined,
    DownOutlined,
    LogoutOutlined,
    FileSearchOutlined,
    UserOutlined,
    SmileOutlined,
    MoneyCollectOutlined,
    BarcodeOutlined,
    BankOutlined,
    AreaChartOutlined,
    DollarCircleOutlined,
} from '@ant-design/icons'
import Image from "next/image"
import sampleImage from "../pages/img/logo.png"
import Head from 'next/head'
import Link from 'next/link'
import { useMediaQuery } from 'react-responsive'

const { SubMenu } = Menu;
const { Header, Sider, Content, Footer } = Layout;

const Template2 = (props) => {
    const [isPhone, setisPhone] = useState(0)
    const isMobile = useMediaQuery({ query: '(max-width: 600px)' })
    useEffect(() => {
        if (isMobile) {
            setisPhone(200)
        } else {
            setisPhone(280)
        }
    }, [isMobile]);
    const Router = useRouter()
    const [collapsed, setCollapsed] = useState(true)
    const toggle = () => {
        setCollapsed(!collapsed)
    }

    const menu = (
        <Menu>
            <Menu.Item key="0">
                <span onClick={() => {
                    localStorage.clear()
                    Router.replace("/")
                    notification.open({
                        message: 'Logout!',
                        description:
                            'You are logout!',
                        icon: <SmileOutlined style={{ color: 'yellow' }} />,
                    });
                }}>
                    <LogoutOutlined /> Sign Out
                </span>
            </Menu.Item>
        </Menu>
    )

    return (
        <>
            <Head>
                <title>ERP</title>
                <meta name="description" content="ERP" />
                <link rel="icon" href="/favicon.ico" />
            </Head>
            <Layout style={{ height: '100vh' }}>
                {
                    collapsed && (
                        <Sider trigger={null} className='coba' style={{ fontWeight: '500' }} width={isPhone}>
                            <div className='coba' style={{ overflow: 'hidden auto', height: '100vh', }}>
                                <div align="center" style={{ minHeight: '50px', margin: '6px', paddingTop: '5px', color: 'white', fontWeight: '400', border: '1px solid #30363d', borderRadius: '5px', background: '#000c17' }}>
                                    <Image src={sampleImage} width="60" height="30" />
                                    <div style={{ marginTop: '-5px', marginBottom: '10px' }}>
                                        PT Tribintang Emas Mulia
                                    </div>
                                </div>
                                <div style={{ border: '1px solid #30363d', margin: '6px 6px 0px 6px', borderRadius: '2px' }}>
                                    <Menu theme="dark" mode="inline" defaultSelectedKeys={[props.kunci]}>
                                        <Menu.Item key="tcode" icon={<FileSearchOutlined />}>
                                            <Link href="/tcode">T-CODE</Link>
                                        </Menu.Item>
                                        <SubMenu key="sales" icon={<UserOutlined />} title="SALES">
                                            <Menu.Item key="custr">
                                                <Link href="/custr">Customers Ranking</Link>
                                            </Menu.Item>
                                            <SubMenu key="stransaction" title="transaction">
                                                <Menu.Item key="sqe">
                                                    <Link href="/sqe">SQ entry</Link>
                                                </Menu.Item>
                                                <Menu.Item key="sqatso">
                                                    <Link href="/sqatso">SQ approval to SO</Link>
                                                </Menu.Item>
                                                <Menu.Item key="sqp">
                                                    <Link href="/sqp">SQ Problems</Link>
                                                </Menu.Item>
                                            </SubMenu>
                                            <SubMenu key="inqrep" title="inquiries and reports">
                                                <Menu.Item key="sqi">
                                                    <Link href="/sqi">SQ Inquiry</Link>
                                                </Menu.Item>
                                            </SubMenu>
                                            <SubMenu key="maint" title="maintenance">
                                                <Menu.Item key="addmancust">
                                                    <Link href="/amc">Add and Manage Cust</Link>
                                                </Menu.Item>
                                            </SubMenu>
                                        </SubMenu>
                                        <SubMenu key="purchases" icon={<MoneyCollectOutlined />} title="PURCHASES">
                                            <Menu.Item key="supr">
                                                <Link href="/supr">Suppliers Ranking</Link>
                                            </Menu.Item>
                                        </SubMenu>
                                        <SubMenu key="itemsinv" icon={<BarcodeOutlined />} title="Items and Inventory">
                                            <Menu.Item key="solir">
                                                <Link href="/solir">Sold Items Ranking</Link>
                                            </Menu.Item>
                                        </SubMenu>
                                        <SubMenu key="bankgl" icon={<BankOutlined />} title="Banking and General Ledger">
                                            <Menu.Item key="clsbal">
                                                <Link href="/clsbal">Class Balances</Link>
                                            </Menu.Item>
                                        </SubMenu>
                                        <SubMenu key="asset" icon={<AreaChartOutlined />} title="ASSET">
                                            <Menu.Item key="assjt">
                                                <Link href="/assjt">Asset Jatuh Tempo</Link>
                                            </Menu.Item>
                                        </SubMenu>
                                        <SubMenu key="promotion" icon={<DollarCircleOutlined />} title="PROMOTION">
                                            <SubMenu key="prtransaction" title="transaction">
                                                <Menu.Item key="transpel">
                                                    <Link href="/transpel">Customer Transaction</Link>
                                                </Menu.Item>
                                            </SubMenu>
                                        </SubMenu>
                                    </Menu>
                                </div>
                            </div>
                        </Sider>
                    )
                }

                <Layout className="site-layout">
                    <Header className="site-layout-background" style={{ padding: 0, borderBottom: '1px solid #1890ff' }}>
                        {React.createElement(collapsed ? MenuUnfoldOutlined : MenuFoldOutlined, {
                            className: 'trigger',
                            onClick: toggle,
                        })}

                        <div style={{ float: 'right', marginRight: '20px' }}>
                            <Dropdown overlay={menu} trigger={['click']} placement="bottomRight">
                                <a style={{ color: 'white' }} onClick={e => e.preventDefault()}>
                                    Admin ERP <DownOutlined />
                                </a>
                            </Dropdown>
                        </div>

                    </Header>
                    <div style={{ overflowY: 'scroll', height: '90%' }}>
                        <Content
                            className="site-layout-backgrounds"
                            style={{
                                margin: '24px 24px 0px 24px',
                                padding: 12,
                                minHeight: 10,
                                border: '1px solid #dee2e6',
                            }}
                        >
                            {props.judul}
                        </Content>
                        <Content
                            className="site-layout-backgrounds"
                            style={{
                                margin: '12px 24px 24px 24px',
                                padding: 24,
                                minHeight: 450,
                                border: '1px solid #dee2e6',
                            }}
                        >
                            {props.konten}
                        </Content>
                    </div>
                    <Footer style={{ padding: '0px', textAlign: 'center' }}>Crafted by TEM &copy; 2022</Footer>
                </Layout>
            </Layout>

        </>
    )
}

export default Template2