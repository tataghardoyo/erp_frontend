import { Form, Input } from 'antd'
import React from 'react'

const PasswordField = (props) => {
    return (
        <>
            <Form.Item
                label={props.label}
                name={props.name}
                rules={[{ required: props.required, message: props.message }]}
            >
                <Input.Password
                    prefix={props.prefix}
                    placeholder={props.plchold}
                />
            </Form.Item>
        </>
    )
}

export default PasswordField