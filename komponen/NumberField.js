import { Form, InputNumber } from 'antd'
import React from 'react'

const NumberField = (props) => {
    return (
        <>
            <Form.Item
                label={props.label}
                name={props.name}
                initialValue={props.dataField}
                rules={[{ required: props.required, message: props.message, }]}
                labelCol={{ span: props.labelcol }}
                wrapperCol={{ span: props.wrappercol }}
            >
                <InputNumber
                    style={{ width: '100%' }}
                    disabled={props.disable}
                    min={props.min} max={props.max}
                    placeholder={props.plchold}
                />
            </Form.Item>
        </>
    )
}

export default NumberField