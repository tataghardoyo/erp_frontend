import { DatePicker, Form } from 'antd'
import React from 'react'

const DateField = (props) => {
    return (
        <>
            <Form.Item
                label={props.label}
                name={props.name}
                initialValue={props.dataField}
                labelCol={{ span: props.labelcol }}
                wrapperCol={{ span: props.wrappercol }}
            >
                <DatePicker style={{ width: '100%' }} format="DD-MM-YYYY" />
            </Form.Item>
        </>
    )
}

export default DateField