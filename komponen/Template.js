import React, { useState } from 'react'
import { useRouter } from 'next/router'
import { Dropdown, Layout, Menu, Tooltip } from 'antd';
import {
    MenuUnfoldOutlined,
    MenuFoldOutlined,
    UserOutlined,
    LaptopOutlined,
    DownOutlined,
    LogoutOutlined,
    FileSearchOutlined,
} from '@ant-design/icons'
import Image from "next/image"
import sampleImage from "../pages/img/logo.png"
import Link from 'next/link'
import Head from 'next/head'
import Loading from './Loading';

const { SubMenu } = Menu;
const { Header, Sider, Content, Footer } = Layout;

const Template = (props) => {
    const [isLoading, setLoading] = useState(false)
    const Router = useRouter()
    const [collapsed, setCollapsed] = useState(false)
    let toggle = () => {
        setCollapsed(!collapsed)
    }
    const menu = (
        <Menu>
            <Menu.Item key="0">
                <span onClick={() => {
                    setLoading(true)
                    localStorage.clear()
                    Router.replace("/")
                }}>
                    <LogoutOutlined /> Sign Out
                </span>
            </Menu.Item>
        </Menu>
    )

    let mql = window.matchMedia('(max-width: 600px)')

    if (isLoading) return (
        <Loading />
    )

    return (
        <>
            <Head>
                <title>ERP</title>
                <meta name="description" content="ERP" />
                <link rel="icon" href="/favicon.ico" />
            </Head>
            <Layout style={{ height: '100vh' }}>
                <Sider collapsedWidth={0} trigger={null} collapsible collapsed={collapsed} width={mql.matches ? 200 : 300} style={{ fontWeight: '500' }}>
                    <div className="logon">
                        <Image src={sampleImage} width="60" height="30" />
                    </div>
                    <Menu theme="dark" mode="inline" defaultSelectedKeys={[props.kunci]} style={{ overflowY: 'scroll', height: '90%' }}>
                        <Menu.Item key="tcode" icon={<FileSearchOutlined />}>
                            <Link href="/">t-code</Link>
                        </Menu.Item>
                        <SubMenu key="sales" icon={<UserOutlined />} title="sales">
                            <SubMenu key="stransaction" title="transaction">
                                <Menu.Item key="sqe">
                                    <Link href="/sqe">
                                        <Tooltip placement="right" title="SQ entry">
                                            <a>SQ entry</a>
                                        </Tooltip>
                                    </Link>
                                </Menu.Item>
                                <Menu.Item key="sqatso">
                                    <Link href="/sqatso">
                                        <Tooltip placement="right" title="SQ approval to SO">
                                            <a>SQ approval to SO</a>
                                        </Tooltip>
                                    </Link>
                                </Menu.Item>
                                <Menu.Item key="sqp">
                                    <Link href="/sqp">
                                        <Tooltip placement="right" title="SQ Problems">
                                            <a>SQ Problems</a>
                                        </Tooltip>
                                    </Link>
                                </Menu.Item>
                            </SubMenu>
                            <SubMenu key="inqrep" title="inquiries and reports">
                                <Menu.Item key="sqi">
                                    <Link href="/sqi">
                                        <Tooltip placement="right" title="SQ Inquiry">
                                            <a>SQ Inquiry</a>
                                        </Tooltip>
                                    </Link>
                                </Menu.Item>
                            </SubMenu>
                            <SubMenu key="maint" title="maintenance">
                                <Menu.Item key="addmancust">
                                    <Link href="/amc">
                                        <Tooltip placement="right" title="Add and Manage Customers">
                                            <a>Add and Manage Cust</a>
                                        </Tooltip>
                                    </Link>
                                </Menu.Item>
                            </SubMenu>
                        </SubMenu>

                        <Menu.Item key="setting" icon={<FileSearchOutlined />}>
                            <Link href="/">
                                <a>settings</a>
                            </Link>
                        </Menu.Item>
                        <SubMenu key="purchases" icon={<LaptopOutlined />} title="purchases">
                            <SubMenu key="ptransaction" title="transaction">
                                <Menu.Item key="pre">
                                    <Link href="/pre">
                                        <Tooltip placement="right" title="Purchases Requisition Entry">
                                            <a>Purchases Requisition Entry</a>
                                        </Tooltip>
                                    </Link>
                                </Menu.Item>
                            </SubMenu>
                        </SubMenu>
                    </Menu>
                </Sider>
                <Layout className="site-layout">
                    <Header className="site-layout-background" style={{ padding: 0 }}>
                        {React.createElement(collapsed ? MenuUnfoldOutlined : MenuFoldOutlined, {
                            className: 'trigger',
                            onClick: toggle,
                        })}

                        <div style={{ float: 'right', marginRight: '20px' }}>
                            <Dropdown overlay={menu} trigger={['click']} placement="bottomRight">
                                <a style={{ color: 'white' }} onClick={e => e.preventDefault()}>
                                    Admin ERP <DownOutlined />
                                </a>
                            </Dropdown>
                        </div>

                    </Header>
                    <div style={{ overflowY: 'scroll', height: '90%' }}>
                        <Content
                            className="site-layout-backgrounds"
                            style={{
                                margin: '24px 24px 0px 24px',
                                padding: 12,
                                minHeight: 10,
                                border: '1px solid #dee2e6',
                            }}
                        >
                            {props.judul}
                        </Content>
                        <Content
                            className="site-layout-backgrounds"
                            style={{
                                margin: '12px 24px 24px 24px',
                                padding: 24,
                                minHeight: 460,
                                border: '1px solid #dee2e6',
                            }}
                        >
                            {props.konten}
                        </Content>
                    </div>
                    <Footer style={{ padding: '0px', textAlign: 'center' }}>Crafted by TEM &copy; 2022</Footer>
                </Layout>
            </Layout>

        </>
    )
}

export default Template