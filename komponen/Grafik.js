import React from 'react'
import {
    Chart as ChartJS,
    CategoryScale,
    LinearScale,
    BarElement,
    Title,
    Tooltip,
    Legend,
} from 'chart.js'

import { Bar } from 'react-chartjs-2'

ChartJS.register(
    CategoryScale,
    LinearScale,
    BarElement,
    Title,
    Tooltip,
    Legend
)

const Grafik = (props) => {

    let datane = props.dataField
    let x = props.x
    let y = props.y

    const labels = datane.map((item) => item[x])
    const dataY = datane.map((item) => item[y])

    const data = {
        labels,
        datasets: [
            {
                label: 'amount',
                data: dataY,
                borderColor: 'rgb(255, 99, 132)',
                backgroundColor: 'rgba(255, 99, 132, 0.5)',
            },

        ],
    }

    const options = {
        indexAxis: 'y',
        elements: {
            bar: {
                borderWidth: 2,
            },
        },
        responsive: true,
        plugins: {
            title: {
                display: true,
                text: props.title,
            },
        },
    }

    return (
        <div style={{marginTop:'20px'}}>
            <Bar options={options} data={data} />
        </div>
    )
}

export default Grafik