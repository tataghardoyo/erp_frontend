let datalistsq = [
    {
        key: '1',
        sq: '84577',
        ref: '02-22-2000008784',
        typejual: 'Locco',
        custcode: '002698',
        customer: 'WARIS TB',
        crscs: 'Good History',
        address: 'JL. TALUN, MUNTILAN, MAGELANG',
        branch: 'WARIS TB',
        area: 'MAGELANG',
        quodate: '31/03/2022',
        valid: '07/04/2022',
        exp: '07/05/2022',
        item: 'SEMEN PADANG PCC 40 KG',
        tonase: '0',
        qtysq: '5',
        qtyaprv: '5',
        qtysisa: '0',
        unit: 'ZAK',
        price: 40500,
        quotot: '202,500',
        ordersisa: '0',
        curr: 'IDR',
        sttsq: '	APROVE',
        isprob: 'No',
        usrent: 'FAIDHI MUSTOFA',
    },
]

export default datalistsq