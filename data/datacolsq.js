import { PrinterOutlined } from "@ant-design/icons"
import { Button } from "antd"

let datacolsq = [
    {
        title: 'SQ',
        dataIndex: 'sq',
        key: 'sq',
        width: 150,
    },
    {
        title: 'Ref',
        dataIndex: 'ref',
        width: 150,
        key: 'ref',
        width: 150,
    },
    {
        title: 'Tipe Jual',
        dataIndex: 'typejual',
        key: 'typejual',
        width: 150,
    },
    {
        title: 'Customer Code',
        dataIndex: 'custcode',
        key: 'custcode',
        width: 150,
    },
    {
        title: 'Customer',
        dataIndex: 'customer',
        key: 'customer',
        width: 150,
    },
    {
        title: 'Credit Status Customer',
        dataIndex: 'crscs',
        key: 'crscs',
        width: 150,
    },
    {
        title: 'Address',
        dataIndex: 'address',
        key: 'address',
        width: 150,
    },
    {
        title: 'Branch',
        dataIndex: 'branch',
        key: 'branch',
        width: 150,
    },
    {
        title: 'Area',
        dataIndex: 'area',
        key: 'area',
        width: 150,
    },
    {
        title: 'Quo Date',
        dataIndex: 'quodate',
        key: 'quodate',
        width: 150,
    },
    {
        title: 'Valid Until',
        dataIndex: 'valid',
        key: 'valid',
        width: 150,
    },
    {
        title: 'Exp Date',
        dataIndex: 'exp',
        key: 'exp',
        width: 150,
    },
    {
        title: 'Item',
        dataIndex: 'item',
        key: 'item',
        width: 150,
    },
    {
        title: 'Tonase',
        dataIndex: 'tonase',
        key: 'tonase',
        width: 150,
    },
    {
        title: 'Qty SQ',
        dataIndex: 'qtysq',
        key: 'qtysq',
        width: 150,
    },
    {
        title: 'Qty Approve',
        dataIndex: 'qtyaprv',
        key: 'qtyaprv',
        width: 150,
    },
    {
        title: 'Qty sisa',
        dataIndex: 'qtysisa',
        key: 'qtysisa',
        width: 150,
    },
    {
        title: 'Unit',
        dataIndex: 'unit',
        key: 'unit',
        width: 150,
    },
    {
        title: 'Price',
        dataIndex: 'price',
        key: 'price',
        width: 150,
    },
    {
        title: 'Quo Total',
        dataIndex: 'quotot',
        key: 'quotot',
        width: 150,
    },
    {
        title: 'Order Sisa',
        dataIndex: 'ordersisa',
        key: 'ordersisa',
        width: 150,
    },
    {
        title: 'Currency',
        dataIndex: 'curr',
        key: 'curr',
        width: 150,
    },
    {
        title: 'Status SQ',
        dataIndex: 'sttsq',
        key: 'sttsq',
        width: 150,
    },
    {
        title: 'Is Problem',
        dataIndex: 'isprob',
        key: 'isprob',
        width: 150,
    },
    {
        title: 'User Entry',
        dataIndex: 'usrent',
        key: 'usrent',
        width: 150,
    },
    {
        title: 'Aksi',
        dataIndex: 'aksi',
        key: 'aksi',
        width: 50,
        render: (address, item) => (
            <Button
                onClick={(e) => {
                    alert(item.sq)
                }}
            >
                <PrinterOutlined />
            </Button>
        ),
    },
]

export default datacolsq