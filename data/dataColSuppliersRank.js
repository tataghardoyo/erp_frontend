let dataColSuppliersRank = [
    {
        title: 'Supplier',
        dataIndex: 'supplier',
        key: 'supplier',
    },
    {
        title: 'Amount',
        dataIndex: 'amount',
        key: 'amount',
        render: (amount, item) => <span>{item.amount.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')}</span>,
    },
]

export default dataColSuppliersRank