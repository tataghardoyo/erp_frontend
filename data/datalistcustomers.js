let dataCustomers = [
    {
        label: 'Putra Perkasa TB - Widodaren Lor RT/RW 004/003, Widodaren, Ngawi, Jawa Timur',
        value: 'C001'
    },
    {
        label: '68 TB - Jl. Krisak Bulu KM 3, Pule, Wng',
        value: 'C002'
    },
    {
        label: 'Bango 99 TB - Dk Delanggu, Klaten',
        value: 'C003'
    }
]

export default dataCustomers