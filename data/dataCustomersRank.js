const dataCustomersRank = [
    {
        key: '1',
        customer: '001151 HIDAYAH 2 TB',
        amount: 444664627.22,
    },
    {
        key: '2',
        customer: '000276 SIDO MULYO 1 TB',
        amount: 410681818.17,
    },
    {
        key: '3',
        customer: '000294 LESTARI TB',
        amount: 266689886.42,
    },
    {
        key: '4',
        customer: '004821 SEMEN INDONESIA (PERSERO) TBK. PT',
        amount: 262403711.82,
    },
    {
        key: '5',
        customer: '002060 AGUNG JAYA TB',
        amount: 247018265.43,
    },
    {
        key: '6',
        customer: '000021 BINTANG ABADI TB',
        amount: 212446579.55,
    },
    {
        key: '7',
        customer: '000957 TUNGGAL MURAH CV',
        amount: 156247454.51,
    },
    {
        key: '8',
        customer: '001808 BERKAH JAYA TB',
        amount: 134156131.79,
    },
    {
        key: '9',
        customer: '000587 WAHANA JATI TB',
        amount: 126455791.92,
    },
    {
        key: '10',
        customer: '002983 RAJAWALI BAROKAH TB',
        amount: 118150192.74,
    },
]

export default dataCustomersRank