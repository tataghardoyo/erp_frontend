let dataSuppliersRank = [
    {
        key: '1',
        supplier: '183 PT SEMEN INDONESIA (PERSERO) TBK*',
        amount: 9543801454.26,
    },
    {
        key: '2',
        supplier: '56 PT SEMEN INDONESIA (PERSERO) TBK',
        amount: 2055170068.16,
    },
    {
        key: '3',
        supplier: '158 AMANAH STATIONARY',
        amount: 262500.00,
    },
    {
        key: '4',
        supplier: '232 CV RUKUN SEJAHTERA',
        amount: 119318.18,
    },
]

export default dataSuppliersRank