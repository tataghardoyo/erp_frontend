let datalistmenu = [
    {
        value: 'sqe',
        label: 'Sales Quotation Entry'
    },
    {
        value: 'sqi',
        label: 'Sales Quotation Inquiry'
    },
    {
        value: 'pre',
        label: 'Purchase Requisition Entry'
    },
    {
        value: 'pra',
        label: 'Purchase Requisition Approval'
    },
]

export default datalistmenu